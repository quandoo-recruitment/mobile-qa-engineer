# Mobile QA Engineer

Hello and thank you for applying for this position!

With this task, we would like to get an idea of how you approach Mobile test automation.

---

### Task Description:

- Please download the latest version of Quandoo consumer mobile app from either iOS App Store or Google Play
(if for some reason you have issues downloading the app, please feel free to use the following [Android apk](https://apkpure.com/quandoo-restaurant-bookings/de.quandoo.android.consumerapp/download?from=details))
- Build a test automation framework covering the following flow:
    - On **Profile** tab, sign up via an email address (please don't use any social buttons to sign up or log in) and fill all the details in order to complete the process
    - Once logged in, navigate to **Discovery** tab and search for `Italian` cuisine in `Berlin`
    - Apply `Open Now` filter
    - Additionaly apply the following filter:
        - Number of people: `4` 
        - Date: the next day
        - Time: any available slot
    - In the retuned list of restaurants please choose the second one and open its details
    - Scroll to `Reviews` section

---

### Goals:
- Working automated test suite
- `README.md` describing the following points:
    - Selected technology stack
    - Reasons behind the chosen framework and pattern(s)
    - How to make the framework work and how to execute the test(s)
    - Next possible steps for improvements

---

Once you’re done please send us the link to a Github/Bitbucket/Gitlab repo (***no zip files will be accepted*** 🛑) with your work.

We know you are busy, so we don’t want to set a strict timeline, but we would much appreciate, if you spend no more than a week.
Our main desire is to see how you think, so please concentrate on quality of your solution rather than on quantity.

If something isn’t clear, just send us an email with your questions! We are willing to help you succeed with the task 😊 

And last, but not least - have fun with it! Good luck 🤞
